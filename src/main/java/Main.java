import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {
    public static void main(String[] args) {
        try {
            // Пример запроса GET.
            URL getUsersUrl = new URL("https://jsonplaceholder.typicode.com/users"); // Создаем URL для GET-запроса.
            HttpURLConnection getUsersConnection = (HttpURLConnection) getUsersUrl.openConnection(); // Открываем соединение.
            getUsersConnection.setRequestMethod("GET"); // Устанавливаем метод запроса.
            // Создаем поток для чтения данных.
            BufferedReader getUsersReader = new BufferedReader(new InputStreamReader(getUsersConnection.getInputStream()));
            String getUsersLine; // Переменная для чтения строки ответа.
            StringBuffer getUsersResponse = new StringBuffer(); // Создаем буфер для хранения ответа.
            while ((getUsersLine = getUsersReader.readLine()) != null) { // Читаем ответ и добавляем его в буфер.
                getUsersResponse.append(getUsersLine);
            }
            getUsersReader.close(); // Закрываем поток чтения.
            System.out.println("GET Users Response: " + getUsersResponse.toString()); // Выводим ответ.

            // Пример запроса POST.
            URL postUserUrl = new URL("https://jsonplaceholder.typicode.com/users"); // Создаем URL для POST-запроса.
            HttpURLConnection postUserConnection = (HttpURLConnection) postUserUrl.openConnection(); // Открываем соединение.
            postUserConnection.setDoOutput(true); // Указываем, что будет использоваться выходной поток.
            postUserConnection.setRequestMethod("POST"); // Устанавливаем метод запроса.
            postUserConnection.setRequestProperty("Content-Type", "application/json"); // Устанавливаем заголовок Content-Type.

            // Данные, которые будут отправлены.
            String postUserData = "{\"name\": \"John Doe\", \"username\": \"johndoe\", \"email\": \"johndoe@example.com\"}";
            try (OutputStream os = postUserConnection.getOutputStream()) { // Создаем поток для записи данных.
                byte[] input = postUserData.getBytes("utf-8"); // Кодируем данные в байты.
                os.write(input, 0, input.length); // Записываем данные в выходной поток.
            }

            // Создаем поток для чтения ответа.
            try (BufferedReader postUserReader = new BufferedReader(new InputStreamReader(postUserConnection.getInputStream()))) {
                String postUserLine; // Переменная для чтения строки ответа.
                StringBuffer postUserResponse = new StringBuffer(); // Создаем буфер для хранения ответа.
                while ((postUserLine = postUserReader.readLine()) != null) { // Читаем ответ и добавляем его в буфер.
                    postUserResponse.append(postUserLine);
                }
                System.out.println("POST User Response: " + postUserResponse.toString()); // Выводим ответ.
            }

            // Пример запроса DELETE.
            URL deleteUserUrl = new URL("https://jsonplaceholder.typicode.com/users/1"); // Создаем URL для DELETE-запроса.
            HttpURLConnection deleteUserConnection = (HttpURLConnection) deleteUserUrl.openConnection(); // Открываем соединение.
            deleteUserConnection.setRequestMethod("DELETE"); // Устанавливаем метод запроса.
            int deleteUserResponseCode = deleteUserConnection.getResponseCode(); // Получаем код ответа.
            System.out.println("DELETE User Response Code: " + deleteUserResponseCode); // Выводим код ответа.
        } catch (Exception e) {
            e.printStackTrace(); // Обрабатываем возможные исключения.
        }
    }
}